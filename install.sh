#/bin/bash
#Full update
sudo apt-get -y update ; 
sudo apt-get -y upgrade ; 
sudo apt-get -y install -f ; 
sudo apt-get -y autoremove ; 
#Installing standard programs
sudo apt-get -y install git curl zsh;
#Google chrome 
sudo apt-get -y install libxss1 libappindicator1 libindicator7 ; 
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb ;
sudo dpkg -i google-chrome*.deb ;
#Spotify 
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys BBEBDCB318AD50EC6865090613B00F1FD2C19886 ; 
echo deb http://repository.spotify.com stable non-free | sudo tee /etc/apt/sources.list.d/spotify.list ; 
sudo apt-get -y update ; 
sudo apt-get -y install spotify-client ;
#Variety
sudo add-apt-repository -y ppa:peterlevi/ppa ;
sudo apt-get -y update ;
sudo apt-get -y install variety variety-slideshow
#Numix installation 
sudo add-apt-repository -y ppa:numix/ppa ; 
sudo apt-get -y update ; 
sudo apt-get -y install numix-gtk-theme numix-icon-theme-circle ; 
gsettings set org.gnome.desktop.interface gtk-theme "Numix" ;
gsettings set org.gnome.desktop.interface icon-theme "Numix-Circle" ; 
#Atom 
sudo add-apt-repository -y ppa:webupd8team/atom ; 
sudo apt-get -y update; 
sudo apt-get -y install atom;
apm install seti-ui monokai-seti open-recent project-manager todo-show minimap highlight-selected minimap-highlight-selected autoclose-html pigments ;
#Python
sudo apt-get -y install python-pip python3-pip;
pip2 install --upgrade pip ;
pip3 install --upgrade pip
echo "try:
    import readline
except ImportError:
    print("Module readline not available.")
else:
    import rlcompleter
    readline.parse_and_bind("tab: complete")" | tee .pythonrc ;
export PYTHONSTARTUP=~/.pythonrc
#Ohmy-zsh 
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)" ;
#Fixing zsh fonts
git clone https://github.com/powerline/fonts.git
fonts/install.sh
rm -rf fonts
#Kill curret user
pkill -KILL -u $(whoami)
